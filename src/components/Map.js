import React, { Component } from "react";
import {
  withGoogleMap,
  GoogleMap,
  withScriptjs,
  Marker,
} from "react-google-maps";
import Geocode from "react-geocode";
import Autocomplete from "react-google-autocomplete";
import { GoogleMapsAPI } from "../google_api";

Geocode.setApiKey(GoogleMapsAPI);
Geocode.enableDebug();

let initialLat;
let initialLng;

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mapPosition: {
        lat: this.props.center.lat,
        lng: this.props.center.lng,
      },
      markerPosition: {
        lat: this.props.center.lat,
        lng: this.props.center.lng,
      },
    };
  }

  onMarkerDragEnd = (event) => {
    let newLat = event.latLng.lat(),
      newLng = event.latLng.lng();

    Geocode.fromLatLng(newLat, newLng).then(
      () => {
        this.setState({
          markerPosition: {
            lat: newLat,
            lng: newLng,
          },
          mapPosition: {
            lat: newLat,
            lng: newLng,
          },
        });
      },
      (error) => {
        console.error(error);
      }
    );
  };

  /* When the user types an address in the search box */
  onPlaceSelected = (place) => {
    // Set these values in the state.
    initialLng = place.geometry.location.lng();
    initialLat = place.geometry.location.lat();
    this.setState({
      markerPosition: {
        lat: initialLat > 0 ? -initialLat : Math.abs(initialLat),
        lng: 180 - Math.abs(initialLng),
      },
      mapPosition: {
        lat: initialLat > 0 ? -initialLat : Math.abs(initialLat),
        lng: 180 - Math.abs(initialLng),
      },
    });
  };

  render() {
    const AsyncMap = withScriptjs(
      withGoogleMap(() => (
        <GoogleMap
          google={this.props.google}
          defaultZoom={this.props.zoom}
          defaultCenter={{
            lat: this.state.mapPosition.lat,
            lng: this.state.mapPosition.lng,
          }}
        >
          {/*Marker*/}
          <Marker
            google={this.props.google}
            name={"Dolores park"}
            draggable={true}
            onDragEnd={this.onMarkerDragEnd}
            position={{
              lat: this.state.markerPosition.lat,
              lng: this.state.markerPosition.lng,
            }}
          />
          <Marker />
          {/* For Auto complete Search Box */}
          <Autocomplete
            style={{
              width: "98.4%",
              height: "40px",
              paddingLeft: "16px",
              marginTop: "20px",
              marginBottom: "30px",
            }}
            onPlaceSelected={this.onPlaceSelected}
            types={["(regions)"]}
          />
          {/* Displays the longitude and latitude for address in the search box. */}
          <div>
            <h1>Address Longitude And Longitude</h1>
            <p>
              <bold>Longitude: </bold>
              {initialLng}
            </p>
            <p>
              <bold>Latitude: </bold>
              {initialLat}
            </p>
          </div>
          {/* Displays the longitude and latitude for the antipode. */}
          <div>
            <h1>Antipode Longitude And Longitude</h1>
            <p>
              <bold>Longitude: </bold>
              {this.state.markerPosition.lng}
            </p>
            <p>
              <bold>Latitude: </bold>
              {this.state.markerPosition.lat}
            </p>
          </div>
        </GoogleMap>
      ))
    );
    let map;
    if (this.props.center.lat !== undefined) {
      map = (
        <div>
          <AsyncMap
            googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${GoogleMapsAPI}&libraries=places`}
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: this.props.height }} />}
            mapElement={<div style={{ height: `100%` }} />}
          />
        </div>
      );
    } else {
      map = <div style={{ height: this.props.height }} />;
    }
    return map;
  }
}
export default Map;
