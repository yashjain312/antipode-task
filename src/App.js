import React from "react";
import Map from "./components/Map";

function App() {
  return (
    <div style={{ marginLeft: "100px", marginRight: "100px" }} className="App">
      <h1>Finding Antipode.</h1>
      <Map center={{ lat: 18.5204, lng: 73.8567 }} height="250px" zoom={4} />
    </div>
  );
}

export default App;
